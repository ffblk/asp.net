﻿using Autofac;
using DataAccess.JsonParsing;

namespace DataAccess
{
    public class DataAccessModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<JsonDataContext>().AsSelf();
        }
    }
}
