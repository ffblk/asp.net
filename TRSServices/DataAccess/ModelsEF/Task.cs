﻿using System;

namespace DataAccess.ModelsEF
{
    public class Task
    {
        public Guid Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public DateTime StartDate { set; get; }
        public DateTime EndDate { set; get; }
        public int StateTask { set; get; }
        public Guid ProjectId { set; get; }
        public Guid Creator { set; get; }

        public Task()
        {
            StartDate = DateTime.UtcNow;
            EndDate = DateTime.UtcNow;
        }
        //public virtual Project Project { get; set; }
        //public virtual User User { get; set; }
        //public virtual TaskReport TaskReport { get; set; }
    }
}
