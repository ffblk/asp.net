﻿using System;

namespace DataAccess.ModelsEF
{
    public class TaskReport
    {
        public Guid Id { set; get; }
        public DateTime DateOfReport { set; get; }
        public DateTime DateEdit { set; get; }
        public int Effort { set; get; }
        public string Description { set; get; }
       
        public Guid TaskId { set; get; }
        public Guid UserId { set; get; }

        public TaskReport()
        {
             DateOfReport = DateTime.UtcNow;
            DateEdit = DateTime.UtcNow;
        }
        //[ForeignKey ("TaskId")]
        //public virtual Task Task { get; set; }
        //public virtual User User { get; set; }
    }
}
