﻿using System;

namespace DataAccess.ModelsEF
{
    public class UserRole
    {
        public Guid Id { set; get; }
        public Guid UserId { get; set; }
        public int RoleId { get; set; }
        public string TaskId { set; get; }
    }
}
