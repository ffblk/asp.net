﻿using System;

namespace DataAccess.ModelsEF
{
    public class Project
    {
        public Guid Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public DateTime StartDate { set; get; }
        public DateTime EndDate { set; get; }
        public Project()
        {
            StartDate = DateTime.UtcNow;
            EndDate = DateTime.UtcNow;
        }
    }
}
