﻿using System.Data.Entity;

namespace DataAccess.ModelsEF
{
    class TRSContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<TaskReport> TakReports { get; set; }
        public  DbSet<User> Users { get; set; }
        public  DbSet<UserRole> UserRoles { get; set; }
        public TRSContext(string connString)
        : base(connString)
        {
        }
    }
}
