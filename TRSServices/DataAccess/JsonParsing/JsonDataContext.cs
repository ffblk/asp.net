﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using System.Xml.Schema;
using AutoMapper;
using DataAccess.ModelsEF;
using Project = DataAccess.Models.Project;
using Task = DataAccess.Models.Task;
using TaskReport = DataAccess.Models.TaskReport;
using User = DataAccess.Models.User;
using UserRole = DataAccess.Models.UserRole;

namespace DataAccess.JsonParsing
{
    public class JsonDataContext
    {
        public static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly string Directory = Path.GetFullPath("..//..//App_Data");
        private static string _type = "";
        private static string _fileName= "";
        public List<Project> Projects { get; set; }
        public List<Task> Tasks { get; set; }
        public List<TaskReport> TaskReports { get; set; }
        public List<User> Users { get; set; }
        public List<UserRole> UserRoles { get; set; }
        public bool IsInitialized { get; private set; }

        public static void ChangeRepository(string fileName)
        {
            //_fileName = fileName;
            var extension = Path.GetExtension(fileName);
            switch (extension)
            {
                case ".json":
                    _type = ".json";
                    _fileName = Directory + "\\" + fileName;
                    break;
                case ".xml":
                    _type = ".xml";
                    _fileName = Directory + "\\" + fileName;
                    break;
                case ".EF":
                    _type = ".EF";
                    int ind = fileName.Length - 3;
                    // вырезаем последний символ
                    _fileName = fileName.Remove(ind);
                    //_fileName -=  ".EF";
                    //_fileName = Path.GetFileNameWithoutExtension(fileName);
                    //_fileName = fileName;//-ef
                    break;
                default:
                    Log.Error("Invalid file format");
                    throw new IOException("Invalid format file");
            }
            //IsInitialized = false;
            
        }
        public JsonDataContext()
        {
            Projects = new List<Project>();
            Tasks = new List<Task>();
            TaskReports = new List<TaskReport>();
            Users = new List<User>();
            UserRoles = new List<UserRole>();
        }

        public void Initialize()
        {
            var extension = Path.GetExtension(_fileName);
            switch (_type)
            {
                case ".json":
                    using (var sr = new StreamReader(_fileName))
                    {
                        var value = sr.ReadToEnd();
                        var data = JsonConvert.DeserializeObject<JsonDataContext>(value);
                        Projects = data.Projects;
                        Tasks = data.Tasks;
                        TaskReports = data.TaskReports;
                        Users = data.Users;
                        UserRoles = data.UserRoles;
                        IsInitialized = true;
                        Log.Info("Reading JSON " + _fileName);
                    }
                    IsInitialized = true;
                    break;
                case ".xml":
                    ReadXml();
                    IsInitialized = true;
                    break;
                case ".EF":
                    ReadEf();
                    IsInitialized = true;
                    break;
                default:
                    Log.Error("Invalid file format");
                    throw new IOException("Invalid format file");
            }
            //IsInitialized = false;
        }
        public void SaveChanges()
        {
            var extension = Path.GetExtension(_fileName);
            switch (_type)
            {
                case ".json":
                    using (var sw = new StreamWriter(_fileName, false))
                    {
                        var value = JsonConvert.SerializeObject(this);
                        sw.Write(value);
                    }
                    break;
                case ".xml":
                    WriteXml(this);
                    break;
                case ".EF":
                    //WriteEf();
                    IsInitialized = true;
                    break;
                default:
                    Log.Error("Invalid file format");
                    throw new IOException("Invalid format file");
            }
        }

        public void ProjectAdd(Project project)
        {
            var extension = Path.GetExtension(_fileName);
            switch (_type)
            {
                case ".json":
                    Projects.Add(project);
                    SaveChanges();
                    break;
                case ".xml":
                    Projects.Add(project);
                    SaveChanges();
                    break;
                case ".EF":
                    using (var db = new TRSContext(_fileName))
                    {
                        var ee = db.Projects.First(c => c.Id == project.Id);
                        if (ee != null)
                        {
                            ee.Description = project.Description;
                            ee.EndDate = project.EndDate;
                            ee.StartDate = project.StartDate;
                            ee.Name = project.Name;
                        }
                        else db.Projects.Add(Mapper.Map<ModelsEF.Project>(project));
                        db.SaveChanges();
                    }
                    break;
                default:
                    Log.Error("Invalid file format");
                    throw new IOException("Invalid format file");
            }
        }

        public void UserRolesAdd(UserRole userRole)
        {
            var extension = Path.GetExtension(_fileName);
            switch (_type)
            {
                case ".json":
                    UserRoles.Add(userRole);
                    SaveChanges();
                    break;
                case ".xml":
                    UserRoles.Add(userRole);
                    SaveChanges();
                    break;
                case ".EF":
                    using (var db = new TRSContext(_fileName))
                    {
                        var ee = db.UserRoles.First(c => c.TaskId == userRole.TaskId && c.UserId == userRole.UserId);
                        if (ee != null)
                        {
                            ee.RoleId = userRole.RoleId;
                        }
                        else db.UserRoles.Add(Mapper.Map<ModelsEF.UserRole>(userRole));
                        db.SaveChanges();
                    }
                    break;
                default:
                    Log.Error("Invalid file format");
                    throw new IOException("Invalid format file");
            }
        }

        public void UserAdd(User user)
        {
            var extension = Path.GetExtension(_fileName);
            switch (_type)
            {
                case ".json":
                    Users.Add(user);
                    SaveChanges();
                    break;
                case ".xml":
                    Users.Add(user);
                    SaveChanges();
                    break;
                case ".EF":
                    using (var db = new TRSContext(_fileName))
                    {
                        var ee = db.Users.First(c => c.Id == user.Id);
                        if (ee != null)
                        {
                            ee.FirstName = user.FirstName;
                            ee.LastName = user.LastName;
                            ee.Locale = user.Locale;
                            ee.RegistrationDate = user.RegistrationDate;
                            ee.TimeZoneUser = user.TimeZoneUser;
                        }
                        else db.Users.Add(Mapper.Map<ModelsEF.User>(user));
                        db.SaveChanges();
                    }
                    break;
                default:
                    Log.Error("Invalid file format");
                    throw new IOException("Invalid format file");
            }
        }

        public void TaskAdd(Task task)
        {
            var extension = Path.GetExtension(_fileName);
            switch (_type)
            {
                case ".json":
                    Tasks.Add(task);
                    SaveChanges();
                    break;
                case ".xml":
                    Tasks.Add(task);
                    SaveChanges();
                    break;
                case ".EF":
                    using (var db = new TRSContext(_fileName))
                    {
                        var ee = db.Tasks.First(c => c.Id == task.Id);
                        if (ee != null)
                        {
                            ee.Creator = task.Creator;
                            ee.Description = task.Description;
                            ee.EndDate = task.EndDate;
                            ee.Name = task.Name;
                            ee.ProjectId = task.ProjectId;
                            ee.StartDate = task.StartDate;
                            ee.StateTask = task.StateTask;
                        }
                        else db.Tasks.Add(Mapper.Map<ModelsEF.Task>(task));
                        db.SaveChanges();
                    }
                    break;
                default:
                    Log.Error("Invalid file format");
                    throw new IOException("Invalid format file");
            }
        }

        public void TaskReportAdd(TaskReport taskReport)
        {
            var extension = Path.GetExtension(_fileName);
            switch (_type)
            {
                case ".json":
                    TaskReports.Add(taskReport);
                    SaveChanges();
                    break;
                case ".xml":
                    TaskReports.Add(taskReport);
                    SaveChanges();
                    break;
                case ".EF":
                    using (var db = new TRSContext(_fileName))
                    {
                        var ee = db.TakReports.First(c => c.Id == taskReport.Id);
                        if (ee != null)
                        {
                            ee.DateEdit = taskReport.DateEdit;
                            ee.DateOfReport = taskReport.DateOfReport;
                            ee.Description = taskReport.Description;
                            ee.Effort = taskReport.Effort;
                            ee.TaskId = taskReport.TaskId;
                            ee.UserId = taskReport.UserId;
                        }
                        else db.TakReports.Add(Mapper.Map<ModelsEF.TaskReport>(taskReport));
                        db.SaveChanges();
                    }
                    break;
                default:
                    Log.Error("Invalid file format");
                    throw new IOException("Invalid format file");
            }
        }
        public void ReadEf()
        {
            using (var db = new TRSContext(_fileName))
            {
                var ee = db.Projects.ToList();
                Projects = Mapper.Map<List<Project>>(ee);
                Tasks = Mapper.Map<List<Task>>(db.Tasks);
                Users = Mapper.Map<List<User>>(db.Users);
                UserRoles = Mapper.Map<List<UserRole>>(db.UserRoles);
                TaskReports = Mapper.Map<List<TaskReport>>(db.TakReports);
            }
        }
        public void ReadXml()
        {
            var custOrdDoc = XDocument.Load(_fileName);
            var schemas = new XmlSchemaSet();
            schemas.Add("", Path.GetDirectoryName(_fileName) + "\\repository-schema.xsd");
            var error = false;
            custOrdDoc.Validate(schemas, (o, e) =>
            {
                Log.Error("Error Validation XML");
                error = true;
            });
            if (error) return;
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(_fileName);
            var xmlRoot = xmlDoc.DocumentElement;
            if (xmlRoot == null)
            {
                Log.Info("XML Repository is Empty");
                return;
            }
            foreach (XmlElement xmlNode in xmlRoot)
            {
                switch (xmlNode.Name)
                {
                    case "Projects":
                        foreach (XmlNode childProjects in xmlNode.ChildNodes)
                        {
                            var project = new Project();
                            foreach (XmlNode childProject in childProjects.ChildNodes)
                            {
                                switch (childProject.Name)
                                {
                                    case "Id":
                                        project.Id = Guid.Parse(childProject.InnerText);
                                        break;
                                    case "Name":
                                        project.Name = childProject.InnerText;
                                        break;
                                    case "Description":
                                        project.Description = childProject.InnerText;
                                        break;
                                    case "StartDate":
                                        project.StartDate = DateTime.Parse(childProject.InnerText);
                                        break;
                                    case "EndDate":
                                        project.EndDate = DateTime.Parse(childProject.InnerText);
                                        break;
                                }
                            }
                            Projects.Add(project);
                        }
                        break;
                    case "Tasks":

                        foreach (XmlNode childProjects in xmlNode.ChildNodes)
                        {
                            var task = new Task();
                            foreach (XmlNode childProject in childProjects.ChildNodes)
                            {
                                switch (childProject.Name)
                                {
                                    case "Id":
                                        task.Id = Guid.Parse(childProject.InnerText);
                                        break;
                                    case "Name":
                                        task.Name = childProject.InnerText;
                                        break;
                                    case "Description":
                                        task.Description = childProject.InnerText;
                                        break;
                                    case "StartDate":
                                        task.StartDate = DateTime.Parse(childProject.InnerText);
                                        break;
                                    case "EndDate":
                                        task.EndDate = DateTime.Parse(childProject.InnerText);
                                        break;
                                    case "StateTask":
                                        task.StateTask = Int32.Parse(childProject.InnerText);
                                        break;
                                    case "ProjectId":
                                        task.ProjectId = Guid.Parse(childProject.InnerText);
                                        break;
                                    case "Creator":
                                        task.Creator = Guid.Parse(childProject.InnerText);
                                        break;
                                }
                            }
                            Tasks.Add(task);
                        }
                        break;
                    case "TaskReports":
                        foreach (XmlNode childProjects in xmlNode.ChildNodes)
                        {
                            var taskReport = new TaskReport();
                            foreach (XmlNode childProject in childProjects.ChildNodes)
                            {
                                switch (childProject.Name)
                                {
                                    case "Id":
                                        taskReport.Id = Guid.Parse(childProject.InnerText);
                                        break;
                                    case "DateOfReport":
                                        taskReport.DateOfReport = DateTime.Parse(childProject.InnerText);
                                        break;
                                    case "DateEdit":
                                        taskReport.DateEdit = DateTime.Parse(childProject.InnerText);
                                        break;
                                    case "Effort":
                                        taskReport.Effort = Int32.Parse(childProject.InnerText);
                                        break;
                                    case "Description":
                                        taskReport.Description = childProject.InnerText;
                                        break;
                                    case "TaskId":
                                        taskReport.TaskId = Guid.Parse(childProject.InnerText);
                                        break;
                                    case "UserId":
                                        taskReport.UserId = Guid.Parse(childProject.InnerText);
                                        break;
                                }
                            }
                            TaskReports.Add(taskReport);
                        }
                        break;
                    case "UserRoles":
                        foreach (XmlNode childProjects in xmlNode.ChildNodes)
                        {
                            var userRole = new UserRole();
                            foreach (XmlNode childProject in childProjects.ChildNodes)
                            {
                                switch (childProject.Name)
                                {
                                    case "UserId":
                                        userRole.UserId = Guid.Parse(childProject.InnerText);
                                        break;
                                    case "RoleId":
                                        userRole.RoleId = Int32.Parse(childProject.InnerText);
                                        break;
                                    case "TaskId":
                                        userRole.TaskId = childProject.InnerText;
                                        break;
                                }
                            }
                            UserRoles.Add(userRole);
                        }
                        break;
                    case "Users":
                        foreach (XmlNode childProjects in xmlNode.ChildNodes)
                        {
                            var user = new User();
                            foreach (XmlNode childProject in childProjects.ChildNodes)
                            {
                                switch (childProject.Name)
                                {
                                    case "Id":
                                        user.Id = Guid.Parse(childProject.InnerText);
                                        break;
                                    case "FirstName":
                                        user.FirstName = childProject.InnerText;
                                        break;
                                    case "LastName":
                                        user.LastName = childProject.InnerText;
                                        break;
                                    case "Locale":
                                        user.Locale = childProject.InnerText;
                                        break;
                                    case "RegistrationDate":
                                        user.RegistrationDate = DateTime.Parse(childProject.InnerText);
                                        break;
                                    case "TimeZoneUser":
                                        user.TimeZoneUser = childProject.InnerText;
                                        break;
                                }
                            }
                            Users.Add(user);
                        }
                        break;
                    case "IsInitialized":
                        IsInitialized = true;
                        break;
                }
            }
        }
        public void WriteXml(JsonDataContext json)
        {
            // The XML
            var xmlr = new XElement("repository",
                new XElement("Projects",
                from pr in this.Projects
                select new XElement("Project",
                    new XElement("Id", pr.Id),
                    new XElement("Name", pr.Name),
                    new XElement("Description", pr.Description),
                    new XElement("StartDate", pr.StartDate),
                    new XElement("EndDate", pr.EndDate)
                    )
                ),
                new XElement("Tasks",
                from ts in this.Tasks
                select new XElement("Task",
                    new XElement("Id", ts.Id),
                    new XElement("Name", ts.Name),
                    new XElement("Description", ts.Description),
                    new XElement("StartDate", ts.StartDate),
                    new XElement("EndDate", ts.EndDate),
                    new XElement("StateTask", ts.StateTask),
                    new XElement("ProjectId", ts.ProjectId),
                    new XElement("Creator", ts.Creator)
                    )
                ),
                new XElement("TaskReports",
                from tr in this.TaskReports
                select new XElement("TaskReport",
                    new XElement("Id", tr.Id),
                    new XElement("DateOfReport", tr.DateOfReport),
                    new XElement("DateEdit", tr.DateEdit),
                    new XElement("Effort", tr.Effort),
                    new XElement("Description", tr.Description),
                    new XElement("TaskId", tr.TaskId),
                    new XElement("UserId", tr.UserId)
                    )
                ),
                new XElement("Users",
                from us in this.Users
                select new XElement("User",
                    new XElement("Id", us.Id),
                    new XElement("FirstName", us.FirstName),
                    new XElement("LastName", us.LastName),
                    new XElement("RegistrationDate", us.RegistrationDate),
                    new XElement("Locale", us.Locale),
                    new XElement("TimeZoneUser", us.TimeZoneUser)
                    )
                ),
                new XElement("UserRoles",
                from ur in this.UserRoles
                select new XElement("UserRole",
                    new XElement("UserId", ur.UserId),
                    new XElement("RoleId", ur.RoleId),
                    new XElement("TaskId", ur.TaskId)
                    )
                )
            );
            xmlr.Save(_fileName);
        }
    }
}