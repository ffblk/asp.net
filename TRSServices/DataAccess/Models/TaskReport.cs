﻿using System;

namespace DataAccess.Models
{
    public class TaskReport
    {
        public Guid Id { set; get; }
        public DateTime DateOfReport { set; get; }
        public DateTime DateEdit { set; get; }
        public int Effort { set; get; }
        public string Description { set; get; }
        public Guid TaskId { set; get; }
        public Guid UserId { set; get; }
    }
}
