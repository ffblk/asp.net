﻿using System;

namespace DataAccess.Models
{
    public class UserRole
    {
        public Guid UserId { get; set; }
        public int RoleId { get; set; }
        public string TaskId { set; get; }
    }
}
