﻿using AutoMapper;
using DataAccess.Models;


namespace DataAccess.Infrastructire
{
    public class DataAccessMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Project, ModelsEF.Project>();
            Mapper.CreateMap<ModelsEF.Project, Project>();

            Mapper.CreateMap<User, ModelsEF.User>();
            Mapper.CreateMap<ModelsEF.User, User>();

            Mapper.CreateMap<Task, ModelsEF.Task>();
            Mapper.CreateMap<ModelsEF.Task, Task>();

            Mapper.CreateMap<TaskReport, ModelsEF.TaskReport>();
            Mapper.CreateMap<ModelsEF.TaskReport, TaskReport>();

            Mapper.CreateMap<UserRole, ModelsEF.UserRole>();
            Mapper.CreateMap<ModelsEF.UserRole, UserRole>();
        }
    }
}
