﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TRSServices.Models;

namespace TRSServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUserRoleService" in both code and config file together.
    [ServiceContract]
    public interface IUserRoleService
    {
        [OperationContract]
        void UpdateRoles(Guid token, IEnumerable<string> users, int role);
        [OperationContract]
        IEnumerable<UserRole> GetUserRoles(Guid userId);
    }
}
