﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TRSServices.Models;
using TRSServices.Repositories;

namespace TRSServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UserService" in both code and config file together.
    [ServiceBehavior(
            InstanceContextMode = InstanceContextMode.Single
        )]
    public class UserService : IUserInfoService
    {
        private readonly IUserInfoRepository _UserRepository;
        public UserService(IUserInfoRepository UserRepository)
        {
            _UserRepository = UserRepository;
        }
        public User ShowUserDetail(Guid id)
        {
            var showingUser = _UserRepository.GetById(id);

            if (showingUser == null) throw new ArgumentException("There is no User with the specified id", nameof(id));

            return showingUser;
        }

        public void EditUser(User user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            _UserRepository.UpdateUser(user);
        }

        public IEnumerable<User> ShowUsers()
        {
            var users = _UserRepository.GetUsers();
            return users;
        }

        public bool ShowUserName(string name)
        {
            return _UserRepository.GetUsers().FirstOrDefault(pr => pr.Name.Equals(name)) != null;
        }

        public User GetById(Guid id)
        {
            var user = _UserRepository.GetById(id);
            return user;
        }

        public string GetType(Guid id)
        {
            return _UserRepository.GetType(id);
        }
    }
}
