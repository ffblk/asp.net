﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TRSServices.Models;

namespace TRSServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITaskReportService" in both code and config file together.
    [ServiceContract]
    public interface ITaskReportService
    {
        [OperationContract]
        IEnumerable<TaskReport> ShowTaskReport(Guid idTask, Guid idUser);
        [OperationContract]
        IEnumerable<TaskReport> ShowAllTaskReport(Guid idTask);
        [OperationContract]
        void AddTaskReport(TaskReport taskReport);
        [OperationContract]
        TaskReport ShowTaskReportDetails(Guid taskReportId);
    }
}
