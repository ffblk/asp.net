﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TRSServices.Models;

namespace TRSServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService2" in both code and config file together.
    [ServiceContract]
    public interface IProjectService
    {
        [OperationContract]
        IEnumerable<Project> ShowProject();
        [OperationContract]
        void EditProject(Project project);

        [OperationContract]
        void ChangeRepository(string filename);
        [OperationContract]
        Project ShowProjectDetails(Guid projectId);
        [OperationContract]
        bool ShowProjectName(string name);
        
    }
}
