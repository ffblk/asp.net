﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TRSServices.Models;

namespace TRSServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IUserService" in both code and config file together.
    [ServiceContract]
    public interface IUserInfoService
    {
        [OperationContract]
        User ShowUserDetail(Guid id);
        [OperationContract]
        void EditUser(User user);
        [OperationContract]
        IEnumerable<User> ShowUsers();
        [OperationContract]
        bool ShowUserName(string name);
        [OperationContract]
        User GetById(Guid id);
        [OperationContract]
        string GetType(Guid id);
    }
}
