﻿using AutoMapper;
using TRSServices.Models;

namespace TRSServices.Infrastructire
{
    public class DomainMapperProfile: Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Project, Project>()
                .ForMember(project => project.Id, opt => opt.Ignore());
            
            Mapper.CreateMap<Project, DataAccess.Models.Project>();
            Mapper.CreateMap<DataAccess.Models.Project, Project>();

            Mapper.CreateMap<Models.Task, DataAccess.Models.Task>();
            Mapper.CreateMap<DataAccess.Models.Task, Models.Task>();

            Mapper.CreateMap<TaskReport, DataAccess.Models.TaskReport>();
            Mapper.CreateMap<DataAccess.Models.TaskReport, TaskReport>();

            Mapper.CreateMap<User, DataAccess.Models.User>();
            Mapper.CreateMap<DataAccess.Models.User, User>();

            Mapper.CreateMap<UserRole, DataAccess.Models.UserRole>();
            Mapper.CreateMap<DataAccess.Models.UserRole, UserRole>();

            Mapper.CreateMap<ConfigHelper.Models.Repository, Repository>();
            Mapper.CreateMap<Repository, ConfigHelper.Models.Repository>();
        }
    }
}
