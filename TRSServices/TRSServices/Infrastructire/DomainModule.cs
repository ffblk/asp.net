﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using TRSServices.Repositories;

namespace TRSServices.Infrastructire
{
    public class DomainModule: Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ProjectService>().As<IProjectService>();
            builder.RegisterType<JsonProjectRepository>().As<IProjectRepository>();

            builder.RegisterType<JsonTaskRepository>().As<ITaskRepository>();
            builder.RegisterType<JsonTaskReportRepository>().As<ITaskReportRepository>();

            builder.RegisterType<TaskService>().As<ITaskService>();
            builder.RegisterType<TaskReportService>().As<ITaskReportService>();

            builder.RegisterType<JsonUserRepository>().As<IUserInfoRepository>();
            builder.RegisterType<JsonUserRoleRepository>().As<IUserRoleRepository>();

            builder.RegisterType<UserService>().As<IUserInfoService>();
            builder.RegisterType<UserRoleService>().As<IUserRoleService>();

            //builder.RegisterType<<IEnumerable<User>>().;
        }
    }
}
