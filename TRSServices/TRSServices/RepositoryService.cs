﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AutoMapper;
using TRSServices.Models;


namespace TRSServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "RepositoryService" in both code and config file together.
    [ServiceBehavior(
           InstanceContextMode = InstanceContextMode.Single,
        IncludeExceptionDetailInFaults = true
       )]
    public class RepositoryService : IRepositoryService
    {
        public IEnumerable<Repository> RepositoryCustomSection()
        {
            return Mapper.Map<IEnumerable<Repository>>(ConfigHelper.XMLReader.WorkWithWebConfig.RepositoryCustomSection());
        }

        public void ChangeRepository(string name)
        {
            //ConfigHelper.XMLReader.WorkWithWebConfig.RepositoryCustomSection();
            DataAccess.JsonParsing.JsonDataContext.ChangeRepository(name);
        }
    }
}
