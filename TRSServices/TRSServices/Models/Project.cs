﻿using System;

namespace TRSServices.Models
{
    public class Project
    {
        public Guid Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public DateTime StartDate { set; get; }
        public DateTime EndDate { set; get; }
        public Project()
        {
            Id = Guid.NewGuid();
            StartDate = DateTime.UtcNow;
            EndDate = DateTime.UtcNow;
        }
        //public List<Guid> ListManager { set; get; }
    }
}
