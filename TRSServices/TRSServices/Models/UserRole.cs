﻿using System;
using System.Runtime.Serialization;

namespace TRSServices.Models
{
    [DataContract]
    public class UserRole
    {
        [DataMember]
        public Guid UserId { get; set; }
        [DataMember]
        public Role RoleId { get; set; }
        [DataMember]
        public string TaskId { set; get; }

        public UserRole(string taskId, int roleId)
        {
            TaskId = taskId;
            RoleId = (Role)roleId;
        }
        public UserRole(string taskId, int roleId, Guid userId)
        {
            UserId = userId;
            TaskId = taskId;
            RoleId = (Role)roleId;
        }
        Role GetRoleId()
        {
            return RoleId;
        }

        string GetTaskId()
        {
            return TaskId;
        }
    }
}
