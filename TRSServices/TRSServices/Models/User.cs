﻿using System;

namespace TRSServices.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Type { get; set; }
       // public Guid Id { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public DateTime RegistrationDate { set; get; }
        public string Locale { set; get; }
        public string TimeZoneUser { set; get; }

        public User()
        {
            Id = Guid.NewGuid();
            RegistrationDate = DateTime.UtcNow;
        }
        public User(ConfigHelper.Models.User u, string fname, string lname, DateTime regDate, string locale, string timeZone)
        {
            Name = u.Name;
            Id = u.Id;
            Password = u.Password;
            Type = u.Type;
            FirstName = fname;
            LastName = lname;
            RegistrationDate = regDate;
            Locale = locale;
            TimeZoneUser = timeZone;
        }
        public User(ConfigHelper.Models.User u)
        {
            Name = u.Name;
            Id = u.Id;
            Password = u.Password;
            Type = u.Type;
            FirstName = "";
            LastName = "";
            RegistrationDate = DateTime.UtcNow;
            Locale = "ru-RU";
            TimeZoneUser = "America/Anchorage";
        }
    }
}
