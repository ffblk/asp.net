﻿using System;
using System.Runtime.Serialization;

namespace TRSServices.Models
{
    [DataContract]
    public class Task
    {
        [DataMember]
        public Guid Id { set; get; }
        [DataMember]
        public string Name { set; get; }
        [DataMember]
        public string Description { set; get; }
        [DataMember]
        public DateTime StartDate { set; get; }
        [DataMember]
        public DateTime EndDate { set; get; }
        [DataMember]
        public State StateTask { set; get; }
        [DataMember]
        public Guid ProjectId { set; get; }
        [DataMember]
        public Guid Creator { set; get; }

        public Task(Guid projectId)
        {
            Id = Guid.NewGuid();
            ProjectId = projectId;
            StartDate = DateTime.UtcNow;
            EndDate = DateTime.UtcNow;
            StateTask = State.Started;
        }
    }
}
