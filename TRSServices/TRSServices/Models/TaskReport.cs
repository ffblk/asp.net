﻿using System;

namespace TRSServices.Models
{
    public class TaskReport
    {
        public Guid Id { set; get; }
        public DateTime DateOfReport { set; get; }
        public DateTime DateEdit { set; get; }        
        public int Effort { set; get; }
        public string Description { set; get; }
        public Guid TaskId { set; get; }
        public Guid UserId { set; get; }
        public TaskReport() { }
        public TaskReport(Guid taskId)
        {
            Id = Guid.NewGuid();
            DateOfReport = DateTime.UtcNow;
            DateEdit = DateTime.UtcNow;
            TaskId = taskId;
        }
    }
    
}
