﻿
namespace TRSServices.Models
{
    public enum State
    {
        Draft,
        Started,
        Completed 
    }
}
