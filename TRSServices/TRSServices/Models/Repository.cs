﻿using System.Runtime.Serialization;

namespace TRSServices.Models
{
    [DataContract]
    public class Repository
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public string Folder { get; set; }
        [DataMember]
        public string Type { get; set; }
        public Repository(string name, string folder, string type)
        {
            Name = name;
            Folder = folder;
            Type = type;
        }
    }
}
