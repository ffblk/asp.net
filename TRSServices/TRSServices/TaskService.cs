﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TRSServices.Models;
using TRSServices.Repositories;

namespace TRSServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TaskService" in both code and config file together.
    [ServiceBehavior(
            InstanceContextMode = InstanceContextMode.Single,
        IncludeExceptionDetailInFaults = true
        )]
    public class TaskService : ITaskService
    {
        private readonly ITaskRepository _taskRepository;
        public TaskService(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public void EditTask(Models.Task task)
        {
            if (task == null) throw new ArgumentNullException(nameof(task));

            _taskRepository.UpdateTask(task);
        }

        public IEnumerable<Models.Task> ShowTasks(Guid id)
        {
            var avaliableTask = _taskRepository.GetTask().Where(tsk => tsk.ProjectId == id).ToList();
            //Claims
            return avaliableTask;
        }

        public Models.Task ShowTaskDetails(Guid taskId)
        {
            var showingTask = _taskRepository.GetTask().FirstOrDefault(pr => pr.Id == taskId);

            if (showingTask == null) return new Task(taskId);//throw new ArgumentException("There is no User with the specified id", nameof(taskId));

            return showingTask;
        }

        public bool ShowTaskName(string name)
        {
            return _taskRepository.GetTask().FirstOrDefault(pr => pr.Name.Equals(name)) != null;
        }
    }
}
