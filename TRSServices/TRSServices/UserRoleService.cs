﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using AutoMapper;
using TRSServices.Models;
using TRSServices.Repositories;

namespace TRSServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "UserRoleService" in both code and config file together.
    [ServiceBehavior(
            InstanceContextMode = InstanceContextMode.Single,
        IncludeExceptionDetailInFaults = true
        )]
    public class UserRoleService : IUserRoleService
    {
        private readonly IUserRoleRepository _userRoleRepository;
        public static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public UserRoleService(IUserRoleRepository userRoleRepository)
        {
            _userRoleRepository = userRoleRepository;

        }
        public void UpdateRoles(Guid token, IEnumerable<string> users, int role)
        {
            Guid guid = new Guid();
            foreach (var e in users.Where(e => Guid.TryParse(e, out guid)))
            {
                _userRoleRepository.UpdateRoles(new UserRole(token.ToString(), role, guid));
                Log.Info("Updates Roles for " + e + ":" + ((Role)role).ToString());
            }
        }

        public IEnumerable<UserRole> GetUserRoles(Guid userId)
        {
            return Mapper.Map<IEnumerable<TRSServices.Models.UserRole>>(_userRoleRepository.GetUserRoles(userId));
        }
    }
}
