﻿using System.Collections.Generic;
using AutoMapper;
using DataAccess.JsonParsing;

namespace TRSServices.Repositories
{
    class JsonTaskRepository : ITaskRepository
    {
        private readonly JsonDataContext _dataContext;
        public JsonTaskRepository(JsonDataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public void AddTask(Models.Task task)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();
            var dataAccessModel = Mapper.Map<DataAccess.Models.Task>(task);
            _dataContext.TaskAdd(dataAccessModel);
            //_dataContext.SaveChanges();
        }
        public IEnumerable<Models.Task> GetTask()
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();
            return Mapper.Map<IEnumerable<Models.Task>>(_dataContext.Tasks);
        }
        public void UpdateTask(Models.Task task)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            _dataContext.Projects.RemoveAll(c => c.Id == task.Id);
            var dataAccessModel = Mapper.Map<DataAccess.Models.Task>(task);

            _dataContext.TaskAdd(dataAccessModel);
            //_dataContext.SaveChanges();
        }
    }
}
