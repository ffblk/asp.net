﻿using System;
using System.Collections.Generic;
using TRSServices.Models;

namespace TRSServices.Repositories
{
    public interface IUserRoleRepository
    {
        IEnumerable<UserRole> GetUserRoles(Guid userId);
        void UpdateRoles(UserRole userRoles);
    }
}
