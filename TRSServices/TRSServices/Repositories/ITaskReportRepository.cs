﻿using System.Collections.Generic;
using TRSServices.Models;

namespace TRSServices.Repositories
{
    public interface ITaskReportRepository
    {
        IEnumerable<TaskReport> GetTaskReport();
        void UpdateTaskReport(TaskReport taskReport);
        void AddTaskReport(TaskReport taskReport);
    }
}
