﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DataAccess.JsonParsing;
using TRSServices.Models;

namespace TRSServices.Repositories
{
    public class JsonUserRoleRepository : IUserRoleRepository
    {
        private readonly JsonDataContext _dataContext;

        public JsonUserRoleRepository(JsonDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IEnumerable<UserRole> GetUserRoles(Guid userId)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            return _dataContext.UserRoles
                .Where(ur => ur.UserId == userId).Select(ur => new UserRole(ur.TaskId.ToString(),ur.RoleId));
        }

        public void UpdateRoles(UserRole userRoles)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            _dataContext.UserRoles.RemoveAll(c => (c.UserId == userRoles.UserId && c.TaskId == userRoles.TaskId));
            var dataAccessModel = Mapper.Map<DataAccess.Models.UserRole>(userRoles);

            _dataContext.UserRolesAdd(dataAccessModel);
            //_dataContext.SaveChanges();
        }
    }
}
