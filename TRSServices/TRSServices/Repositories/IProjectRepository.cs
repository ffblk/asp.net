﻿using System.Collections.Generic;
using TRSServices.Models;

namespace TRSServices.Repositories
{
    public interface IProjectRepository
    {
        IEnumerable<Project> GetProject();
        void UpdateProject(Project project);
        void AddProject(Project project);
    }
}
