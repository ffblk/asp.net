﻿using System.Collections.Generic;

namespace TRSServices.Repositories
{
    public interface ITaskRepository
    {
        IEnumerable<Models.Task> GetTask();
        void UpdateTask(Models.Task task);
        void AddTask(Models.Task task);
    }
}
