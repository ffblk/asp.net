﻿using System.Collections.Generic;
using AutoMapper;
using DataAccess.JsonParsing;
using TRSServices.Models;

namespace TRSServices.Repositories
{
    public class JsonProjectRepository : IProjectRepository
    {
        private readonly JsonDataContext _dataContext;

        public JsonProjectRepository(JsonDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void AddProject(Project project)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();
            var dataAccessModel = Mapper.Map<DataAccess.Models.Project>(project);
            _dataContext.Projects.Add(dataAccessModel);
            _dataContext.SaveChanges();
        }

        public IEnumerable<Project> GetProject()
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();
            
            return Mapper.Map<IEnumerable<Project>>(_dataContext.Projects);
        }

        public void UpdateProject(Project project)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            _dataContext.Projects.RemoveAll(c => c.Id == project.Id);
            var dataAccessModel = Mapper.Map<DataAccess.Models.Project>(project);

            _dataContext.Projects.Add(dataAccessModel);
            _dataContext.SaveChanges();
        }

    }
}
