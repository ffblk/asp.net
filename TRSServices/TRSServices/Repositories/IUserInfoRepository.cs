﻿using System;
using System.Collections.Generic;
using TRSServices.Models;

namespace TRSServices.Repositories
{
    public interface IUserInfoRepository
    {
        IEnumerable<User> GetUsers();
        User GetById(Guid id);
        void UpdateUser(User user);
        string GetType(Guid id);
        string GetTimeZone(Guid id);
    }
}
