﻿using System.Collections.Generic;
using AutoMapper;
using DataAccess.JsonParsing;
using TRSServices.Models;

namespace TRSServices.Repositories
{
    class JsonTaskReportRepository : ITaskReportRepository
    {
        private readonly JsonDataContext _dataContext;

        public JsonTaskReportRepository(JsonDataContext dataContext)
        {
            _dataContext = dataContext;
        }
        public void AddTaskReport(TaskReport taskReport)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();
            var dataAccessModel = Mapper.Map<DataAccess.Models.TaskReport>(taskReport);
            _dataContext.TaskReportAdd(dataAccessModel);
            //_dataContext.SaveChanges();
        }

        public IEnumerable<TaskReport> GetTaskReport()
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            return Mapper.Map<IEnumerable<TaskReport>>(_dataContext.TaskReports);
        }

        public void UpdateTaskReport(TaskReport taskReport)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            _dataContext.TaskReports.RemoveAll(c => c.Id == taskReport.Id);
            var dataAccessModel = Mapper.Map<DataAccess.Models.TaskReport>(taskReport);

            _dataContext.TaskReportAdd(dataAccessModel);
            //_dataContext.SaveChanges();
        }
    }
}
