﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using DataAccess.JsonParsing;
using TRSServices.Models;

namespace TRSServices.Repositories
{
    public class JsonUserRepository : IUserInfoRepository
    {
        private readonly JsonDataContext _context;
        private readonly ConfigHelper.Repositories.IUserRepository _users;

        public JsonUserRepository(JsonDataContext context, ConfigHelper.Repositories.IUserRepository users)
        {
            _context = context;
            _users = users;
        }

        public IEnumerable<User> GetUsers()
        {
            if (!_context.IsInitialized) _context.Initialize();
            
            var xmlUserInfo = _users.GetUsers();
            var userFullInfo = new List<User>();
            var userInfo = _context.Users;
            foreach (var i in xmlUserInfo)
            {
                if (userInfo != null)
                {
                    var f = userInfo.FirstOrDefault(u => u.Id == i.Id);
                    if (f != null)
                        userFullInfo.Add(new User(i, f.FirstName, f.LastName, f.RegistrationDate, f.Locale,
                            f.TimeZoneUser));
                    else userFullInfo.Add(new User(i));
                }
                else userFullInfo.Add(new User(i));
            }
            return userFullInfo;

            //return Mapper.Map<IEnumerable<User>>(_context.Users);
        }

        public User GetById(Guid id)
        {
            if (!_context.IsInitialized) _context.Initialize();
            var xmlUserInfo = _users.GetById(id);// WorkWithWebConfig.UserCustomSection().FirstOrDefault(u => u.Id == id);
            var userInfo = _context.Users.FirstOrDefault(u => u.Id == id);
            return userInfo != null
                ? new User(xmlUserInfo, userInfo.FirstName, userInfo.LastName,
                    userInfo.RegistrationDate, userInfo.Locale, userInfo.TimeZoneUser)
                : new User(xmlUserInfo);

            //var dataAccessUser = _context.Users.FirstOrDefault(u => u.Id == id);
            //return Mapper.Map<User>(dataAccessUser);
        }

        public void UpdateUser(User user)
        {
            if (!_context.IsInitialized) _context.Initialize();

            _context.Users.RemoveAll(c => c.Id == user.Id);
            var dataAccessModel = Mapper.Map<DataAccess.Models.User>(user);

            _context.UserAdd(dataAccessModel);
            //_context.SaveChanges();
        }

        public string GetType(Guid id)
        {
            return _users.GetType(id);
        }

        public string GetTimeZone(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}
