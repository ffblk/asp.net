﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TRSServices.Models;

namespace TRSServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ITaskService" in both code and config file together.
    [ServiceContract]
    public interface ITaskService
    {
        [OperationContract]
        IEnumerable<Task> ShowTasks(Guid id);
        [OperationContract]
        void EditTask(Task task);
        [OperationContract]
        Task ShowTaskDetails(Guid taskId);
        [OperationContract]
        bool ShowTaskName(string name);
    }
}
