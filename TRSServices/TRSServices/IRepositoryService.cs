﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Text;
using TRSServices.Models;


namespace TRSServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IRepositoryService" in both code and config file together.
    [ServiceContract]
    public interface IRepositoryService
    {
        [OperationContract]
        IEnumerable<Repository> RepositoryCustomSection();
        [OperationContract]
        void ChangeRepository(string name);
    }
}
