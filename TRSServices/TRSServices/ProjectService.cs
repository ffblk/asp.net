﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using DataAccess.JsonParsing;
using TRSServices.Models;
using TRSServices.Repositories;

namespace TRSServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service2" in both code and config file together.
    [ServiceBehavior(
            InstanceContextMode = InstanceContextMode.Single
        )]
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;
        public static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public ProjectService(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }
        public IEnumerable<Project> ShowProject()
        {
            var avaliableProjects = _projectRepository.GetProject().ToList();
            Log.Info("Return All Project");
            return avaliableProjects;
        }
        public void EditProject(Project project)
        {
            if (project == null)
            {
                Log.Error("Argument Null Exception, Project Incorrect");
                throw new ArgumentNullException(nameof(project));
            }

            _projectRepository.UpdateProject(project);
            Log.Info("Update Project " + project.Id.ToString());
        }

        public void ChangeRepository(string filename)
        {
            JsonDataContext.ChangeRepository(filename);
        }

        public Project ShowProjectDetails(Guid projectId)
        {
            var showingProject = _projectRepository.GetProject().FirstOrDefault(pr => pr.Id == projectId);

            if (showingProject == null)
            {
                Log.Error("Argument Exception, There is no Project with the specified id");
                throw new ArgumentException("There is no Project with the specified id", nameof(projectId));
            }

            return showingProject;
        }

        public bool ShowProjectName(string name)
        {
            
            //return true if est' takoy false if netu
            return _projectRepository.GetProject().FirstOrDefault(pr => pr.Name.Equals(name)) != null;
        }
    }
}
