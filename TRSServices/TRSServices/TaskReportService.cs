﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TRSServices.Models;
using TRSServices.Repositories;

namespace TRSServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "TaskReportService" in both code and config file together.
    [ServiceBehavior(
            InstanceContextMode = InstanceContextMode.Single
        )]
    public class TaskReportService : ITaskReportService
    {
        private readonly ITaskReportRepository _TaskReportRepository;
        public TaskReportService(ITaskReportRepository TaskReportRepository)
        {
            _TaskReportRepository = TaskReportRepository;
        }

        public void AddTaskReport(TaskReport taskReport)
        {
            if (taskReport == null) throw new ArgumentNullException(nameof(taskReport));

            _TaskReportRepository.UpdateTaskReport(taskReport);
        }

        public IEnumerable<TaskReport> ShowAllTaskReport(Guid idTask)
        {
            var avaliableProjects = _TaskReportRepository.GetTaskReport().Where(tsk => (tsk.TaskId == idTask)).ToList();
            return avaliableProjects;
        }

        public IEnumerable<TaskReport> ShowTaskReport(Guid idTask, Guid idUser)
        {
            var avaliableProjects = _TaskReportRepository.GetTaskReport().Where(tsk => (tsk.TaskId == idTask && tsk.UserId == idUser)).ToList();
            return avaliableProjects;
        }

        public TaskReport ShowTaskReportDetails(Guid taskReportId)
        {
            var showingTaskReport = _TaskReportRepository.GetTaskReport().FirstOrDefault(pr => pr.Id == taskReportId);
            //zdes' kakbi doljna bit' proverka est' li task s takim ID, kaknibud', no eto posle
            if (showingTaskReport == null) throw new ArgumentException("There is no User with the specified id", nameof(taskReportId));

            return showingTaskReport;
        }
    }
}
