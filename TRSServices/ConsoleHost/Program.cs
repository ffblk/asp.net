﻿using System;
using System.Web;
using System.ServiceModel;
using Autofac;
using AutoMapper;
using TRSServices;
using TRSServices.Start;
using Autofac.Integration.Wcf;
using ConfigHelper;
using DataAccess;
using DataAccess.JsonParsing;
using TRSServices.Infrastructire;
using System.IO;

namespace ConsoleHost
{
    class Program
    {
        static void Main(string[] args)
        {
            JsonDataContext.ChangeRepository("Data.json");
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule<DataAccessModule>();
            builder.RegisterModule<ConfigHelperModule>();
            builder.RegisterModule<DomainModule>();
            var container = builder.Build();
            // Get the SingleInstance from the container.
            var service = container.Resolve<IProjectService>();
            var serviceHostProject = new ServiceHost(service);
            var service1 = container.Resolve<ITaskReportService>();
            var serviceHostTaskReport = new ServiceHost(service1);
            var service2 = container.Resolve<ITaskService>();
            var serviceHostTask = new ServiceHost(service2);
            var service3 = container.Resolve<IUserInfoService>();
            var serviceHostUserInfo = new ServiceHost(service3);
            var service4 = container.Resolve<IUserRoleService>();
            var serviceHostUserRole = new ServiceHost(service4);
            //var service5 = container.Resolve<IRepositoryService>();
            //var serviceHostRepository = new ServiceHost(service5);
            var serviceHostRepository = new ServiceHost(typeof (RepositoryService));

            ServiceHost serviceHost = new ServiceHost(typeof(TRSServices.Service1));
            try
            {
                Mapper.AddProfile(new TRSServices.Infrastructire.DomainMapperProfile());
                //Instantiate ServiceHost
                serviceHostProject.Open();
                serviceHost.Open();
                serviceHostTaskReport.Open();
                serviceHostTask.Open();
                serviceHostUserInfo.Open();
                serviceHostUserRole.Open();
                serviceHostRepository.Open();
                //var dd = System.Web.HttpContext.Current.Server;
                Console.WriteLine("Service is live now");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is an issue with StudentService" + ex.Message);
            }


        }
    }
}
namespace TRSServices.Start
{
    using Autofac;
    using ConfigHelper;
    using DataAccess;
    public class AutofacConfig
    {
        public static void Config()
        {
            var builder = new ContainerBuilder();
            //builder
            //      .Register(c => c.Resolve<ChannelFactory<IProjectService>>().CreateChannel())
            //      .As<ProjectService>();
            builder.RegisterType<ProjectService>().As<IProjectService>();
            builder.RegisterType<TRSServices.IProjectService>();
            builder.RegisterModule<DataAccessModule>();
            //builder.RegisterModule<DomainModule>();
            builder.RegisterModule<ConfigHelperModule>();
            
            var container = builder.Build();
            //DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}
