﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using ConfigHelper.ConfigSection;
using ConfigHelper.Models;

namespace ConfigHelper.XMLReader
{
    public class WorkWithWebConfig
    {
        public static IEnumerable<Repository> RepositoryCustomSection()
        {
            // Get the application configuration file.
            var config = System.Web.HttpContext.Current 
                != null ? System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~/Views/") 
                : ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            // Read the custom section.
            var group = config.GetSectionGroup("customGroup");
            if (group == null) throw new Exception("Net userov v userah");
            var col = group.Sections;
            var myCustomGroupConfig = col["repositories"]as RepositoriesConfig;
            if (myCustomGroupConfig == null) throw new Exception("Net userov v userah");
            return (from RepositoryConfigElement e in myCustomGroupConfig.Repositories select new Repository(e.Name, e.Folder, e.Type)).ToList();
        }
        public static IEnumerable<User> UserCustomSection()
        {
            // Get the application configuration file.
            var config = System.Web.HttpContext.Current 
                != null ? System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~") 
                : ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            // Read the custom section.
            var group = config.GetSectionGroup("customGroup");
            if (group == null) throw new Exception("Net userov v userah");
            var col = group.Sections;
            var myCustomGroupConfig = col["users"] as UsersConfig;
            if (myCustomGroupConfig == null) throw new Exception("Net userov v userah");
            return (from UserConfigElement e in myCustomGroupConfig.Users select new User(e.Id, e.Name, e.Password, e.Type)).ToList();

        }
    }
}