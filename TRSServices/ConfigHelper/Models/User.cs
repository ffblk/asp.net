﻿using System;

namespace ConfigHelper.Models
{
    public class User
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Type { get; set; }

        public User(Guid id, string name, string password, string type)
        {
            Id = id;
            Name = name;
            Password = password;
            Type = type;
        }
        public User() { }
    }
}
