﻿using System;
using System.Runtime.CompilerServices;

namespace ConfigHelper.Models
{
    public class UserFull :  User
    {
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public DateTime RegistrationDate { set; get; }
        public string Locale { set; get; }
        public string TimeZoneUser { set; get; }
        public UserFull()
        { }
        public UserFull(User u, string fname, string lname, DateTime regDate, string locale, string timeZone)
        {
            Name = u.Name;
            Id = u.Id;
            Password = u.Password;
            Type = u.Type;
            FirstName = fname;
            LastName = lname;
            RegistrationDate = regDate;
            Locale = locale;
            TimeZoneUser = timeZone;
        }
        public UserFull(User u)
        {
            Name = u.Name;
            Id = u.Id;
            Password = u.Password;
            Type = u.Type;
            FirstName = "";
            LastName = "";
            RegistrationDate = DateTime.UtcNow;
            Locale = "ru-RU";
            TimeZoneUser = "America/Anchorage";
        }
    }
}
