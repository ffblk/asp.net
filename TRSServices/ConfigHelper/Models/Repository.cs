﻿namespace ConfigHelper.Models
{
    public class Repository
    {
        public string Name { get; set; }
        public string Folder { get; set; }
        public string Type { get; set; }
        public Repository(string name, string folder, string type)
        {
            Name = name;
            Folder = folder;
            Type = type;
        }
    }
}
