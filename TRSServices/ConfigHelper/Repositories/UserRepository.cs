﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ConfigHelper.Models;
using ConfigHelper.XMLReader;
//using User = TRSServices.Models.User;

namespace ConfigHelper.Repositories
{
    public class UserRepository : IUserRepository
    {
        //private readonly TRSServices.Repositories.IUserInfoRepository _userRepository;
        public UserRepository(/*TRSServices.Repositories.IUserInfoRepository userRepository*/)
        {
            //userRepository = userRepository;
        }
        public IEnumerable<User>GetUsers()
        {
            var xmlUserInfo = WorkWithWebConfig.UserCustomSection();
            //var userFullInfo = new List<UserFull>();
            //var userInfo = _userRepository.GetUsers();
            //foreach (var i in xmlUserInfo)
            //{
            //    if (userInfo != null)
            //    {
            //        var f = userInfo.FirstOrDefault(u => u.Id == i.Id);
            //        if (f != null)
            //            userFullInfo.Add(new UserFull(i, f.FirstName, f.LastName, f.RegistrationDate, f.Locale,
            //                f.TimeZoneUser));
            //        else userFullInfo.Add(new UserFull(i));
            //    }
            //    else userFullInfo.Add(new UserFull(i));
            //}
            return xmlUserInfo;
        }
        public User GetById(Guid id)
        {
            var xmlUserInfo = WorkWithWebConfig.UserCustomSection().FirstOrDefault(u => u.Id == id);
            //var userInfo = _userRepository.GetUsers().FirstOrDefault(u => u.Id == id);
            //return userInfo != null
            //    ? new UserFull(xmlUserInfo, userInfo.FirstName, userInfo.LastName,
            //        userInfo.RegistrationDate, userInfo.Locale, userInfo.TimeZoneUser)
            //    : new UserFull(xmlUserInfo);
            return xmlUserInfo;
        }
        public void UpdateUser(UserFull user)
        {
            //_userRepository.UpdateUser(Mapper.Map<User>(user));
            throw new NotImplementedException("q");
        }
        public string GetType(Guid id)
        {
            //error
            return WorkWithWebConfig.UserCustomSection().FirstOrDefault(u => u.Id == id).Type;
        }
        public string GetTimeZone(Guid id)
        {
            throw new NotImplementedException("d");
            //return _userRepository.GetById(id).TimeZoneUser;
        }
    }
}
