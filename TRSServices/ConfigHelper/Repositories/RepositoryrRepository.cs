﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConfigHelper.Models;
using ConfigHelper.XMLReader;

namespace ConfigHelper.Repositories
{
    public class RepositoryRepository : IRepositoryRepository
    {
        public RepositoryRepository()
        {
        }
        public IEnumerable<Repository> GetRepositories()
        {
            var repositories = WorkWithWebConfig.RepositoryCustomSection();
            return repositories;
        }
        public Repository GetById(string id)
        {
            var xmlRepositoryInfo = WorkWithWebConfig.RepositoryCustomSection()
                .FirstOrDefault(u => u.Folder.Equals(id));
            return xmlRepositoryInfo;
        }
        public void UpdateRepository(Repository repository)
        {
            throw new NotImplementedException("q");
        }
    }
}
