﻿using System;
using System.Collections.Generic;
using ConfigHelper.Models;

namespace ConfigHelper.Repositories
{
    public interface IRepositoryRepository
    {
        IEnumerable<Repository> GetRepositories();
        Repository GetById(string id);
        void UpdateRepository(Repository repository);
    }
}
