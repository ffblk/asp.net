﻿using System;
using System.Collections.Generic;
using ConfigHelper.Models;

namespace ConfigHelper.Repositories
{
    public interface IUserRepository
    {
        IEnumerable<User>GetUsers();
        User GetById(Guid id);
        void UpdateUser(UserFull user);
        string GetType(Guid id);
        string GetTimeZone(Guid id);
    }
}
