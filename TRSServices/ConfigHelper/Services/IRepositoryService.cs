﻿using System;
using System.Collections.Generic;
using ConfigHelper.Models;

namespace ConfigHelper.Services
{
    public interface IRepositoryService
    {
        Repository ShowRepositoryDetail(string id);
        void EditRepository(Repository user);
        IEnumerable<Repository> ShowRepositories();
        bool ShowRepositoryName(string name);
    }
}
