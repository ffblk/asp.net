﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConfigHelper.Models;
using ConfigHelper.Repositories;

namespace ConfigHelper.Services
{
    public class RepositoryService : IRepositoryService
    {
        private readonly IRepositoryRepository _repositoryRepository;

        public RepositoryService(IRepositoryRepository repositoryRepository)
        {
            _repositoryRepository = repositoryRepository;
        }
        public Repository ShowRepositoryDetail(string id)
        {
            var showingUser = _repositoryRepository.GetById(id);
            if (showingUser == null) throw new ArgumentException("There is no User with the specified id", nameof(id));
            return showingUser;
        }

        public void EditRepository(Repository user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            _repositoryRepository.UpdateRepository(user);
        }

        public IEnumerable<Repository> ShowRepositories()
        {
            var users = _repositoryRepository.GetRepositories();
            return users;
        }

        public bool ShowRepositoryName(string name)
        {
            return _repositoryRepository.GetRepositories().FirstOrDefault(pr => pr.Name.Equals(name)) != null;
        }
    }
}
