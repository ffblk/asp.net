﻿using System;
using System.Collections.Generic;
using System.Linq;
using ConfigHelper.Models;
using ConfigHelper.Repositories;

namespace ConfigHelper.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public User ShowUserDetail(Guid id)
        {
            var showingUser = _userRepository.GetById(id);

            if (showingUser == null) throw new ArgumentException("There is no User with the specified id", nameof(id));

            return showingUser;
        }

        public void EditUser(UserFull user)
        {
            if (user == null) throw new ArgumentNullException(nameof(user));

            _userRepository.UpdateUser(user);
        }

        public IEnumerable<User> ShowUsers()
        {
            var users = _userRepository.GetUsers();
            return users;
        }

        public bool ShowUserName(string name)
        {
            return _userRepository.GetUsers().FirstOrDefault(pr => pr.Name.Equals(name)) != null;
        }
    }
}
