﻿using System;
using System.Collections.Generic;
using ConfigHelper.Models;

namespace ConfigHelper.Services
{
    public interface IUserService
    {
        User ShowUserDetail(Guid id);
        void EditUser(UserFull user);
        IEnumerable<User> ShowUsers();
        bool ShowUserName(string name);
    }
}
