﻿using System;
using System.Configuration;

namespace ConfigHelper.ConfigSection
{
    public class UserConfigElement : ConfigurationElement
    {
        public UserConfigElement(Guid token, string name, string folder, string type)
        {
            this.Id = token;
            this.Name = name;
            this.Password = folder;
            this.Type = type;
        }
        public UserConfigElement()
        {
        }
        [ConfigurationProperty("guid", IsRequired = true)]
        public Guid Id
        {
            get
            {
                Guid gd ;
                Guid.TryParse(this["guid"].ToString(), out gd);
                return gd;
            }
            private set
            {
                this["guid"] = value;
            }
        }
        [ConfigurationProperty("name", DefaultValue = "User", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }
        [ConfigurationProperty("password", DefaultValue = "11111", IsRequired = true)]
        public string Password
        {
            get
            {
                return (string)this["password"];
            }
            set
            {
                this["password"] = value;
            }
        }
        [ConfigurationProperty("type", DefaultValue = "user", IsRequired = true)]
        public string Type
        {
            get
            {
                return (string)this["type"];
            }
            set
            {
                this["type"] = value;
            }
        }
    }
}