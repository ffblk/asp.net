﻿using System;
using System.Configuration;

namespace ConfigHelper.ConfigSection
{
    public class RepositoriesCollections : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new RepositoryConfigElement();
        }
        protected override string ElementName => "repository";
        public RepositoryConfigElement this[int index]
        {
            get
            {
                return (RepositoryConfigElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }
        new public RepositoryConfigElement this[string name] => (RepositoryConfigElement)BaseGet(name);
        public void Add(RepositoryConfigElement url)
        {
            BaseAdd(url);
        }
        public void Remove(RepositoryConfigElement repository)
        {
            if (BaseIndexOf(repository) >= 0)
            {
                BaseRemove(repository.Name);
            }
        }
        public void Clear()
        {
            BaseClear();
        }
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((RepositoryConfigElement)element).Name;
        }
    }
}