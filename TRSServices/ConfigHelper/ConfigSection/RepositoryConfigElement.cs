﻿using System;
using System.Configuration;

namespace ConfigHelper.ConfigSection
{
    public class RepositoryConfigElement : ConfigurationElement
    {
        public RepositoryConfigElement(string name, string folder, string type)
        {
            this.Name = name;
            this.Folder = folder;
            this.Type = type;
        }
        public RepositoryConfigElement()
        {
        }
        [ConfigurationProperty("name", DefaultValue = "Projects", IsRequired = true)]
        public string Name
        {
            get
            {
                return (string)this["name"];
            }
            set
            {
                this["name"] = value;
            }
        }
        [ConfigurationProperty("folder", DefaultValue = "Data", IsRequired = true)]
        public string Folder
        {
            get
            {
                return (string)this["folder"];
            }
            set
            {
                this["folder"] = value;
            }
        }
        [ConfigurationProperty("type", DefaultValue = "json", IsRequired = true)]
        [RegexStringValidator(@"(json|xml|EF)")]
        public string Type
        {
            get
            {
                return (string)this["type"];
            }
            set
            {
                this["type"] = value;
            }
        }
    }
}