﻿using System;
using System.Configuration;

namespace ConfigHelper.ConfigSection
{
    public class UsersCollections : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new UserConfigElement();
        }
        public UserConfigElement this[int index]
        {
            get
            {
                return (UserConfigElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }
        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((UserConfigElement)element).Name;
        }
    }
}