﻿using System.Configuration;

namespace ConfigHelper.ConfigSection
{
    public class UsersConfig : ConfigurationSection
    {
        [ConfigurationProperty("users", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(UsersCollections), AddItemName = "user", ClearItemsName = "clear", RemoveItemName = "remove")]
        public UsersCollections Users
        {
            get
            {
                UsersCollections usersCollections =
                    (UsersCollections)base["users"];
                return usersCollections;
            }
            set
            {
                UsersCollections usersCollections = value;
            }
        }
    }
}