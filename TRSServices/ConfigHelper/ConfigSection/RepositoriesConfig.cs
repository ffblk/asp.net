﻿using System.Configuration;

namespace ConfigHelper.ConfigSection
{
    public class RepositoriesConfig : ConfigurationSection
    {
        [ConfigurationProperty("repositories", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(RepositoriesCollections), AddItemName = "repository")]
        public RepositoriesCollections Repositories
        {
            get
            {
                return (RepositoriesCollections)this["repositories"];
            }
            set
            {
                var repositoriesCollections = value;
            }
        }
    }
}