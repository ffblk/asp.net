﻿using Autofac;
using ConfigHelper.Repositories;
using ConfigHelper.Services;
namespace ConfigHelper
{
    public class ConfigHelperModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UserRepository>().As<IUserRepository>();
            builder.RegisterType<RepositoryRepository>().As<IRepositoryRepository>();
            builder.RegisterType<RepositoryService>().As<IRepositoryService>();
            builder.RegisterType<UserService>().As<IUserService>();
        }
    }
}
