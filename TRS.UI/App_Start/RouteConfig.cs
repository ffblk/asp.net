﻿using System.Web.Mvc;
using System.Web.Routing;

namespace TRS.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    "View Profile",
            //    "Profile/{id}",
            //    new
            //    {
            //        controller = "Profile",
            //        action = "Index",
            //        id = UrlParameter.Optional
            //    });
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
