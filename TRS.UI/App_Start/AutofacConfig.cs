﻿using Autofac;
using Autofac.Integration.Mvc;

namespace TRS.UI.App_Start
{
    using Infrastructure;
    using System.Web.Mvc;

    public class AutofacConfig
    {
        public static void Config()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
            builder.RegisterModule<UIModule>();
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}