﻿using AutoMapper;
using TRS.UI.Models;

namespace TRS.UI.App_Start
{
    public class UIMapperProfile: Profile
    {
        protected override void Configure()
        {
            //Mapper.CreateMap<Domain.Models.Project, ProjectViewModel>();
            //Mapper.CreateMap<ProjectViewModel, Domain.Models.Project>();
            Mapper.CreateMap<TaskViewModel, TRS.UI.ServiceReferenceTask.Task>();
            Mapper.CreateMap<TRS.UI.ServiceReferenceTask.Task, TaskViewModel>();

            Mapper.CreateMap<ServiceReferenceTaskReport.TaskReport, TaskReportViewModel>()
                .ForMember(dest => dest.Task_Id,
                    opts => opts.MapFrom(src => src.TaskId));
            Mapper.CreateMap<TaskReportViewModel, ServiceReferenceTaskReport.TaskReport>()
                .ForMember(dest => dest.TaskId,
                    opts => opts.MapFrom(src => src.Task_Id));
            Mapper.CreateMap<TRS.UI.ServiceReferenceUser.User, UserFullModel>()
                .ForMember(dest => dest.UserName,
                    opts => opts.MapFrom(src => src.Name));
            Mapper.CreateMap<UserFullModel, TRS.UI.ServiceReferenceUser.User>()
                .ForMember(dest => dest.Name,
                    opts => opts.MapFrom(src => src.UserName));
            Mapper.CreateMap<UserViewModel, TRS.UI.ServiceReferenceUser.User>()
                .ForMember(dest => dest.Name,
                    opts => opts.MapFrom(src => src.UserName));
            Mapper.CreateMap<TRS.UI.ServiceReferenceUser.User, UserViewModel>()
                .ForMember(dest => dest.UserName,
                    opts => opts.MapFrom(src => src.Name));
            Mapper.CreateMap<UserViewModel, TRS.UI.ServiceReferenceUser.User>()
                .ForMember(dest => dest.Name,
                    opts => opts.MapFrom(src => src.UserName));

            //Mapper.CreateMap<RepositoryVewModel, TRS.ConfigHelper.Models.Repository>();
            //Mapper.CreateMap<TRS.ConfigHelper.Models.Repository, RepositoryVewModel>();

            Mapper.CreateMap<RepositoryVewModel, ServiceReferenceRepository.Repository>();
            Mapper.CreateMap<ServiceReferenceRepository.Repository, RepositoryVewModel>();

            Mapper.CreateMap<UserHelperModel, TRS.UI.ServiceReferenceUser.User>();
            Mapper.CreateMap<TRS.UI.ServiceReferenceUser.User, UserHelperModel>();

            //TRS.UI.ServiceReferenceProject.Project->TRS.UI.Models.ProjectViewModel
            Mapper.CreateMap<TRS.UI.ServiceReferenceProject.Project, TRS.UI.Models.ProjectViewModel>();
            Mapper.CreateMap<TRS.UI.Models.ProjectViewModel, TRS.UI.ServiceReferenceProject.Project>();
            //Mapper.CreateMap<>()

            Mapper.CreateMap<TaskViewModel,ServiceReferenceTask.Task>();
            Mapper.CreateMap<ServiceReferenceTask.Task,TaskViewModel>();
            
            //Mapper.CreateMap<RepositoryConfigElement,>()
            base.Configure();
        }
    }
}