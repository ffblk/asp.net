﻿using System.Web.Mvc;
using TRS.UI.Infrastructure;

namespace TRS.UI.App_Start
{
    public class FilterConfig
    {
        public static void RegisterFilters(GlobalFilterCollection filters)
        {
            filters.Add(new LocalizationFilter());
        }
    }
}