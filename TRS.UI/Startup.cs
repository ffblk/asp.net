﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(TRS.UI.Startup))]

namespace TRS.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}