﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using TRS.UI.Models;
using AutoMapper;
//using TRS.ConfigHelper.Repositories;
using Task = System.Threading.Tasks.Task;

namespace TRS.UI.Authentication
{
    public class CustomUserStore : IUserPasswordStore<UserViewModel, string>,
        IUserClaimStore<UserViewModel, string>
    {
        private readonly TRS.UI.ServiceReferenceUser.IUserInfoService _userRepository;
        public CustomUserStore()
        {
            _userRepository = new ServiceReferenceUser.UserInfoServiceClient();
        }

        public void Dispose()
        {
        }

        public Task CreateAsync(UserViewModel user)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(UserViewModel user)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(UserViewModel user)
        {
            throw new NotImplementedException();
        }

        public Task<UserViewModel> FindByIdAsync(string userId)
        {
            var user = _userRepository.GetById(Guid.Parse(userId));

            var viewModel = Mapper.Map<UserViewModel>(user);

            return Task.FromResult(viewModel);
        }
        public Task<string> GetLocaleByIdAsunc(string userId)
        {
            var user = _userRepository.GetById(Guid.Parse(userId));

            return Task.FromResult(user.Locale);
        }

        public Task<UserViewModel> FindByNameAsync(string userName)
        {
            var user = _userRepository.ShowUsers()
                .FirstOrDefault(u => u.Name.Equals(userName, StringComparison.OrdinalIgnoreCase));
            var viewModel = Mapper.Map<UserViewModel>(user);

            return Task.FromResult(viewModel);
        }

        public Task SetPasswordHashAsync(UserViewModel user, string passwordHash)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetPasswordHashAsync(UserViewModel user)
        {
            var stored = _userRepository.GetById(Guid.Parse(user.Id));

            return Task.FromResult(new PasswordHasher().HashPassword(stored.Password));
        }

        public Task<bool> HasPasswordAsync(UserViewModel user)
        {
            return Task.FromResult(true);
        }

        public Task AddToRoleAsync(UserViewModel user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task RemoveFromRoleAsync(UserViewModel user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task<IList<string>> GetRolesAsync(UserViewModel user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsInRoleAsync(UserViewModel user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task<IList<Claim>> GetClaimsAsync(UserViewModel user)
        {
            //var roles = _userRoleRepository.GetUserRoles(new Guid(user.Id));
            //IList<Claim> claims = roles
            //    .Select(r => new Claim(r.RoleId.ToString(), r.TaskId.ToString()))
            //    .ToList();
            //GetType(new Guid(user.Id))
            //claims.Add(new Claim("locale", (_userRepository.GetById(Guid.Parse(user.Id)).Locale).ToString()));
            //claims.Add(new Claim("TimeZone", _userRepository.GetTimeZone(new Guid(user.Id)) ));
            IList<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Role, _userRepository.GetType(new Guid(user.Id))));
            return Task.FromResult(claims);
        }

        public Task RemoveClaimAsync(UserViewModel user, Claim claim)
        {
            throw new NotImplementedException();
        }

        public Task AddClaimAsync(UserViewModel user, Claim claim)
        {
            throw new NotImplementedException();
        }
    }

}