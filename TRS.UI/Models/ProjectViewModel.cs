﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TRS.UI.Models
{
    public class ProjectViewModel
    {
        public Guid Id { set; get; }
        [Required]
        public string Name { set; get; }
        [Required]
        public string Description { set; get; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { set; get; }
        [Required]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { set; get; }
        [Required]
        public IEnumerable<string> Managers { set; get; } 
    }
}