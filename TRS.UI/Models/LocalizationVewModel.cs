﻿namespace TRS.UI.Models
{
    public class LocalizationVewModel
    {
        public string LocalizationId { get; set; }
        public string Name { get; set; }
    }
}