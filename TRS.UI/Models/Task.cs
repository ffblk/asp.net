﻿using System;

namespace TRS.UI.Models
{
    public class Task
    {
        public Guid Id { set; get; }
        public string Name { set; get; }
        public string Description { set; get; }
        public DateTime StartDate { set; get; }
        public DateTime EndDate { set; get; }
        public int StateTask { set; get; }
        public Guid ProjectId { set; get; }
        public Guid Creator { set; get; }
    }
}
