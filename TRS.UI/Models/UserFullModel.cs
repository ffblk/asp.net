﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TRS.UI.Models
{
    public class UserFullModel
    {
        public string Id { set; get; }
        [Required(ErrorMessageResourceType = typeof(Resources.Errors), ErrorMessageResourceName = "NoField")]
        [StringLength(15, MinimumLength = 5, ErrorMessageResourceType = typeof(Resources.Errors), ErrorMessageResourceName = "SetLogin")]
        public string UserName { set; get; } //Login
        [Required(ErrorMessageResourceType = typeof(Resources.Errors), ErrorMessageResourceName = "NoField")]
        [DataType(DataType.Password)]
        public string Password { set; get; }
       [Required(ErrorMessageResourceType = typeof(Resources.Errors), ErrorMessageResourceName = "NoField")]
      [RegularExpression(@"\D+", ErrorMessageResourceType = typeof(Resources.Errors), ErrorMessageResourceName = "SetFirstName")]
        public string FirstName { set; get; }
       [Required(ErrorMessageResourceType = typeof(Resources.Errors), ErrorMessageResourceName = "NoField")]
        [RegularExpression(@"\D+", ErrorMessageResourceType = typeof(Resources.Errors), ErrorMessageResourceName = "SetLastName")]
        public string LastName { set; get; }
        public DateTime RegistrationDate { set; get; }
        [Required]
        public string Locale { set; get; }
        [Required]
        public string TimeZoneUser { set; get; }
    }
}