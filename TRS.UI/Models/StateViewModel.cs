﻿namespace TRS.UI.Models
{
    public class StateViewModel
    {
        public int StateId { get; set; }
        public string Name { get; set; }
    }
}