﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace TRS.UI.Models
{
    public class TaskViewModel
    {
        public Guid Id { set; get; }
        [Required]
        public string Name { set; get; }
        [Required]
        public string Description { set; get; }
        [Required]
        public DateTime StartDate { set; get; }
        [Required]
        public DateTime EndDate { set; get; }
        public State StateTask { set; get; }
        [Required]
        public Guid ProjectId { set; get; }
        public Guid Creator { set; get; }
        [Required]
        public IEnumerable<string> Managers { set; get; }
    }
    public enum State
    {
        Draft,
        Started,
        Completed
    }
}