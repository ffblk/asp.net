﻿namespace TRS.UI.Models
{
    public class RepositoryVewModel
    {
        public string Name { get; set; }
        public string Folder { get; set; }
        public string Type { get; set; }
    }
}