﻿namespace TRS.UI.Models
{
    public class TimeZoneViewModel
    {
        public string TimeZoneId { get; set; }
        public string Name { get; set; }

        public TimeZoneViewModel(string timeZoneId, string name)
        {
            TimeZoneId = timeZoneId;
            Name = name;
        }
    }
}