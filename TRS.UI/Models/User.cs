﻿using System;

namespace TRS.DataAccess.Models
{
    public class User
    {
        public Guid Id { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public DateTime RegistrationDate { set; get; }
        public string Locale { set; get; }
        public string TimeZoneUser { set ; get; }
    }
}
