﻿using System;
using System.ComponentModel.DataAnnotations;

namespace TRS.UI.Models
{
    public class TaskReportViewModel
    {
        public Guid Id { set; get; }
        [Required]
        public DateTime DateOfReport { set; get; }
        [Required]
        public DateTime DateEdit { set; get; }
        [Required]
        public int Effort { set; get; }
        [Required]
        public string Description { set; get; }
        public Guid Task_Id { set; get; } //i don't know pochemu s TaskId ne rabotaet
        public Guid UserId { set; get; }
    }
}