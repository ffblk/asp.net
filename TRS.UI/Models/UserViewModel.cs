﻿using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace TRS.UI.Models
{
    public class UserViewModel : IUser<string>
    {
        public string Id { set; get; }
        public string UserName { set; get; } //Login

        //public UserViewModel(string id, string userName)
        //{
        //    Id = id;
        //    UserName = userName;
        //}
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<UserViewModel, string> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}