﻿using System.Web.Mvc;
using System.Web.Routing;
using AutoMapper;

using TRS.UI.App_Start;
using System.IO;


namespace TRS.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static readonly log4net.ILog Log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            FilterConfig.RegisterFilters(GlobalFilters.Filters);

            Mapper.AddProfile(new UIMapperProfile());
            //var container = new UnityContainer();
            //container
            //    .RegisterType<ServiceReferenceProject.IProjectService, ServiceReferenceProject.ProjectServiceClient> ()
            //    .Configure<InjectedMembers>()
            //    .ConfigureInjectionFor<ServiceReferenceProject.ProjectServiceClient>(new InjectionConstructor("*"));
            //DependencyResolver.SetResolver(new UnityDependencyResolver(container));

            AutofacConfig.Config();
            var tt = new ServiceReferenceRepository.RepositoryServiceClient();
            tt.ChangeRepository("Data.json");
            //tt.ChangeRepository("Data Source = (localdb)\\v11.0; Initial Catalog = TRS.DataAccess.ModelsEF.TRSContext; Integrated Security = True.EF");
            //JsonDataContext.ChangeRepository(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/Data.json"));
            log4net.Config.XmlConfigurator.Configure(new FileInfo(Server.MapPath("~/Views/web.config")));
            TRS.UI.MvcApplication.Log.Info("App Started");
        }
    }

}
