﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TRS.UI.Models;

namespace TRS.UI.Infrastructure
{
    public static class TimeZoneHelper
    {
        static TimeZoneHelper()
        {
            TimeZones = new List<TimeZoneViewModel>();
            ReadOnlyCollection<TimeZoneInfo> tz = TimeZoneInfo.GetSystemTimeZones();
            foreach (var i in tz)
            {
                TimeZones.Add(new TimeZoneViewModel(i.Id, i.DisplayName));
            }
            //TimeZones
        }
        private static List<TimeZoneViewModel> TimeZones; 
        public static IEnumerable<TimeZoneViewModel> GetAvaliableTimeZone()
        {
            return TimeZones;
        }
    }

}