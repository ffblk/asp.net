﻿using System.Collections.Generic;
using AutoMapper;
using TRS.UI.Models;

namespace TRS.UI.Infrastructure
{
    public static class RepositoryHelper
    {
        public static IEnumerable<RepositoryVewModel> GetAvaliableRepositories()
        {
            var tt = new ServiceReferenceRepository.RepositoryServiceClient();
            return Mapper.Map<IEnumerable<RepositoryVewModel>>(tt.RepositoryCustomSection());
        }
    }
}