﻿using System.Collections.Generic;
using TRS.UI.Models;

namespace TRS.UI.Infrastructure
{
    public static class LocalizationHelper
    {
        private static readonly LocalizationVewModel[] Localizations =
        {
            new LocalizationVewModel {LocalizationId = "en-GB", Name = "English"},
            new LocalizationVewModel {LocalizationId = "ru-RU", Name = "Русский"},
            new LocalizationVewModel {LocalizationId = "be-BY", Name = "Беларуская"}
        };

        public static IEnumerable<LocalizationVewModel> GetAvaliableLocalizations()
        {
            return Localizations;
        }
    }
}