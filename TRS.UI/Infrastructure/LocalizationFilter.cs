﻿using System;
using System.Globalization;
using System.Threading;
using System.Web.Mvc;

namespace TRS.UI.Infrastructure
{
    public class LocalizationFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var lang = filterContext.RequestContext.HttpContext.Request.Cookies.Get("Lang");
            if (lang != null)
            {

                try
                {
                    var culture = new CultureInfo(lang.Value);
                    Thread.CurrentThread.CurrentUICulture = culture;
                }
                catch (Exception)
                {
                    // TODO: LOG
                }
            }

            base.OnActionExecuting(filterContext);
        }
    }
}