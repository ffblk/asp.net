﻿using System.Collections.Generic;
using TRS.UI.Models;

namespace TRS.UI.Infrastructure
{
    public static class StateHelper
    {
        private static readonly StateViewModel[] states =
        {
            new StateViewModel {StateId = 0, Name = "Draft"},
            new StateViewModel {StateId = 1, Name = "Started"},
            new StateViewModel {StateId = 2, Name = "Completed"}
        };

        public static IEnumerable<StateViewModel> GetAvaliableLocalizations()
        {
            return states;
        }
    }
}