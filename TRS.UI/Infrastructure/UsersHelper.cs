﻿using System.Collections.Generic;
using AutoMapper;

using TRS.UI.Models;


namespace TRS.UI.Infrastructure
{
    public class UsersHelper
    {
        public static IEnumerable<UserHelperModel> GetAvaliableLocalizations(ServiceReferenceUser.IUserInfoService users)
        {
            return Mapper.Map<IEnumerable<UserHelperModel>>(users.ShowUsers());
        }
    }
}