﻿using Autofac;
using Microsoft.AspNet.Identity;
using System.Web;
using TRS.UI.Authentication;
using TRS.UI.Models;

namespace TRS.UI.Infrastructure
{
    public class UIModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {

            builder.RegisterType<UserManager<UserViewModel, string>>().AsSelf();
            builder.RegisterType<CustomUserStore>().As<IUserStore<UserViewModel, string>>() ;
            //builder.RegisterType<TRS.UI.ServiceReferenceProject.IProjectService>(ServiceReferenceProject.ProjectServiceClient);
            //Kernel.Bind<UserManager<UserViewModel, string>>().ToSelf();
            //Kernel.Bind<IUserStore<UserViewModel, string>>().To<CustomUserStore>();

            builder.Register(ctx => HttpContext.Current != null
                ? HttpContext.Current.Server.MapPath("~/App_Data/Data.json")
                : null).Named<string>("DataFile");

            //Kernel.Bind<string>().ToMethod(ctx => HttpContext.Current != null
            //    ? HttpContext.Current.Server.MapPath("~/App_Data/Data.json")
            //    : null);

        }
    }
}