﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using TRS.UI.Models;
using TRS.UI.ServiceReferenceUser;

namespace TRS.UI.Controllers
{
    public class ProfileController : Controller
    {
        private readonly ServiceReferenceUser.IUserInfoService _userService;

        public ProfileController()
        {
            _userService = new UserInfoServiceClient();
        }
        public ActionResult ViewProfile(string token)
        {
            var user = _userService.ShowUserDetail(Guid.Parse(token));

            var viewModel = Mapper.Map<UserFullModel>(user);
            MvcApplication.Log.Info("View profile user" + user.Id);
            return View(viewModel);
        }
        public ActionResult Create(string token)
        {
            var user = token != null ? _userService.ShowUserDetail(Guid.Parse(token)) : new User();

            var viewModel = Mapper.Map<UserFullModel>(user);
            MvcApplication.Log.Info("Create User");
            return View(viewModel);
        }

        public ActionResult ViewAll()
        {
            IEnumerable<User> users = _userService.ShowUsers();
            MvcApplication.Log.Info("View all users");
            return View(Mapper.Map<IEnumerable<UserFullModel>>(users));
        }
        [HttpPost]
        public ActionResult ViewProfile(UserFullModel userView)
        {
            return RedirectToAction("ProfileChange", "Login", new {loc = userView.Locale, time = userView.TimeZoneUser});
        }
        [HttpPost]
        public ActionResult Create(UserFullModel user)
        {
            if (!ModelState.IsValid)
            {
                return View(user);
            }
            var domainTask = Mapper.Map<User>(user);
            _userService.EditUser(domainTask);
            MvcApplication.Log.Info("Create New user");
            return RedirectToAction("Index", "Home");
        }
    }
}