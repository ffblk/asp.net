﻿using System;
using System.Web.Mvc;

namespace TRS.UI.Controllers
{
    public class RepositoryController : Controller
    {
        //private readonly JsonDataContext _dataContext;

        public RepositoryController(/*JsonDataContext dataContext*/)
        {
            //_dataContext = dataContext;
        }
        // GET: Repository
        [HttpPost]
        public ActionResult SetRepository(string name, string returnUrl = null)
        {
            try
            {
                var changeRep = new ServiceReferenceProject.ProjectServiceClient();
                MvcApplication.Log.Info("Change repository on " + name);
                changeRep.ChangeRepository(name);
                return RedirectToAction("Relogin", "Login", new { returnUrl = returnUrl });
            }
            catch (Exception exc)
            {
                MvcApplication.Log.Error("Error change repository "+exc.Message);
            }
            return Redirect(returnUrl);
        }
    }
}