﻿using AutoMapper;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading;
using System.Web.Mvc;
using TRS.UI.Models;


namespace TRS.UI.Controllers
{
    public class TaskReportController : Controller
    {
        private readonly ServiceReferenceTaskReport.ITaskReportService _taskReportService;
        // GET: TaskReport
        public TaskReportController(/*ITaskReportService taskReportService*/)
        {
            _taskReportService = new ServiceReferenceTaskReport.TaskReportServiceClient();
        }
        public ActionResult CreateTaskReport(Guid token)
        {
            var currentUser = Thread.CurrentPrincipal.Identity as ClaimsIdentity;
            var taskReport = _taskReportService.ShowTaskReportDetails(token);
            taskReport.UserId = Guid.Parse(currentUser.GetUserId());
            var _taskReport = Mapper.Map<TaskReportViewModel>(taskReport);
            return View(_taskReport);
        }

        public ActionResult ShowTaskReport(Guid token, string project)
        {
            ServiceReferenceTaskReport.TaskReport[] tasksReport;
            var currentUser  = Thread.CurrentPrincipal.Identity as ClaimsIdentity;
            if (currentUser.HasClaim("Manager", project) 
                || currentUser.HasClaim(ClaimTypes.Role, "admin"))
                tasksReport = _taskReportService.ShowAllTaskReport(token);
            else if (currentUser.HasClaim("User", token.ToString()))
                tasksReport = _taskReportService.ShowTaskReport(token, Guid.Parse(currentUser.GetUserId()));

            else throw new NotImplementedException("ACCESS DENIED");
            var view = Mapper.Map<IEnumerable<TaskReportViewModel>>(tasksReport);
            return View(view);
        }
        [HttpPost]
        public ActionResult CreateTaskReport(TaskReportViewModel taskReport)
        {
            if (!ModelState.IsValid)
            {
                return View(taskReport);
            }
            taskReport.DateEdit=DateTime.Now;
            var domainTaskReport = Mapper.Map<ServiceReferenceTaskReport.TaskReport>(taskReport);
            _taskReportService.AddTaskReport(domainTaskReport);
            return RedirectToAction("Index","Home");
        }
    }
}