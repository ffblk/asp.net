﻿using System.Collections.Generic;
using System.Web.Mvc;
using TRS.UI.Models;
using AutoMapper;

namespace TRS.UI.Controllers
{
    public class ProjectController : Controller
    {
        // GET: Project
        private readonly ServiceReferenceProject.IProjectService _projectService;
        public ProjectController(/*ServiceReferenceProject.IProjectService projectService*/)
        {
            _projectService = new ServiceReferenceProject.ProjectServiceClient();
        }
        public ActionResult Index()
        {
            var projectList = _projectService.ShowProject();
            var viewModel = Mapper.Map<IEnumerable<ProjectViewModel>>(projectList);
            MvcApplication.Log.Info("View projects");
            return View(viewModel);
        }
    }
}