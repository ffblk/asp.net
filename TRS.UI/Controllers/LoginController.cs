﻿using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TRS.UI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System.Globalization;
using System.Security.Claims;
using System;
using System.Collections.Generic;
using TRS.UI.ServiceReferenceUser;

namespace TRS.UI.Controllers
{
    public class LoginController : Controller
    {
        private readonly UserManager<UserViewModel, string> _userManager;
        private readonly ServiceReferenceUserRole.IUserRoleService _userRoleRepository;
        private readonly ServiceReferenceUser.IUserInfoService _userRepository;

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;
        public LoginController(UserManager<UserViewModel, string> userManager)
        {
            _userRepository = new UserInfoServiceClient();
            _userManager = userManager;
            _userRoleRepository = new ServiceReferenceUserRole.UserRoleServiceClient();
        }
        public PartialViewResult Login()
        {
            return PartialView(new LoginViewModel());
        }
        [HttpPost]
        public async Task<ActionResult> Loging(LoginViewModel login)
        {
            var user = await _userManager.FindAsync(login.UserName, login.Password);
            if (user == null)
            {
                MvcApplication.Log.Error("Login failed, USER : " + login.UserName);
                return RedirectToAction("Unauthorized");
            }
            var identity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            
            //Create Claim local for repository
            var roles = _userRoleRepository.GetUserRoles(new Guid(user.Id));
            var roles1 = AutoMapper.Mapper.Map<IEnumerable<UserRole>>(roles);
            IList<Claim> claims = roles1
                .Select(r => new Claim(r.RoleId.ToString(), r.TaskId.ToString()))
                .ToList();
            identity.AddClaims(claims);
            identity.AddClaim(new Claim("locale", (_userRepository.GetById(Guid.Parse(user.Id)).Locale).ToString()));
            identity.AddClaim(new Claim("TimeZone", (_userRepository.GetById(Guid.Parse(user.Id)).TimeZoneUser).ToString()));
            MvcApplication.Log.Info("Create Custom Claim local for repository");
            AuthenticationManager.SignIn(identity);
            if (identity.Claims.Count(c => c.Type == "locale") != 0)
            {
                var cultureInfo = new CultureInfo(identity.Claims.Where(c => c.Type == "locale").Select(c => c.Value).SingleOrDefault());
                Response.Cookies.Add(new HttpCookie("Lang", cultureInfo.Name));
                var timeZone = identity.Claims.Where(c => c.Type == "TimeZone").Select(c => c.Value).SingleOrDefault();
                Response.Cookies.Add(new HttpCookie("Time", timeZone));
                MvcApplication.Log.Info("Change Language "+ cultureInfo);
            }
            else
            {
                var cultureInfo = new CultureInfo("ru-RU");
                Response.Cookies.Add(new HttpCookie("Lang", cultureInfo.Name));
                Response.Cookies.Add(new HttpCookie("Time", TimeZoneInfo.Local.DisplayName));
                MvcApplication.Log.Info("Change Language " + cultureInfo);
            }

            MvcApplication.Log.Info("Login successful USER : " + login.UserName);
           
            return RedirectToAction("Index", "Home");
        }
        [Authorize]
        public async Task<ActionResult> Relogin(string returnUrl)
        {
            var currentUser = User.Identity as ClaimsIdentity;
            var ee= new UserViewModel();
            ee.Id = currentUser.Claims.Where(
                c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")
                .Select(c => c.Value).Single();
            ee.UserName = currentUser.Name;
            //var user = new UserViewModel(, );
            var identity = await _userManager.CreateIdentityAsync(ee, DefaultAuthenticationTypes.ApplicationCookie);

            //Create Claim local for repository
            Claim claim;
            if (identity.Claims.Count(c => c.Type == "locale") != 0)
            {
                claim = (from c in identity.Claims
                         where c.Type == "locale"
                         select c).Single();
                identity.RemoveClaim(claim);
            }
            if (identity.Claims.Count(c => c.Type == "TimeZone") != 0)
            {
                claim = (from c in identity.Claims
                         where c.Type == "TimeZone"
                         select c).Single();
                identity.RemoveClaim(claim);
            }
            MvcApplication.Log.Info("Delete claim");
            var id =
                identity.Claims.Where(
                    c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")
                    .Select(c => c.Value).Single();
            //Create Claim local for repository
            var roles = _userRoleRepository.GetUserRoles(new Guid(id));
            var roles1 = AutoMapper.Mapper.Map<IEnumerable<UserRole>>(roles);
            IList<Claim> claims = roles1
                .Select(r => new Claim(r.RoleId.ToString(), r.TaskId.ToString()))
                .ToList();
            identity.AddClaims(claims);
            identity.AddClaim(new Claim("locale", (_userRepository.GetById(Guid.Parse(id)).Locale).ToString()));
            identity.AddClaim(new Claim("TimeZone", (_userRepository.GetById(Guid.Parse(id)).TimeZoneUser).ToString()));
            AuthenticationManager.SignIn(identity);
            MvcApplication.Log.Info("Create Custom Claim local for repository");
            if (identity.Claims.Count(c => c.Type == "locale") != 0)
            {
                var cultureInfo = new CultureInfo(identity.Claims.Where(c => c.Type == "locale").Select(c => c.Value).SingleOrDefault());
                Response.Cookies.Add(new HttpCookie("Lang", cultureInfo.Name));
                var timeZone = identity.Claims.Where(c => c.Type == "TimeZone").Select(c => c.Value).SingleOrDefault();
                Response.Cookies.Add(new HttpCookie("Time",timeZone));
                MvcApplication.Log.Info("Change Language " + cultureInfo);
            }
            else
            {
                var cultureInfo = new CultureInfo("ru-RU");
                Response.Cookies.Add(new HttpCookie("Lang", cultureInfo.Name));
                Response.Cookies.Add(new HttpCookie("Time", TimeZoneInfo.Local.DisplayName));
                MvcApplication.Log.Info("Change Language " + cultureInfo);
            }

            MvcApplication.Log.Info("Login successful USER : " + identity.Name);
            return Redirect(returnUrl);
        }
        [Authorize]
        public async Task<ActionResult> ProfileChange(string loc, string time)
        {
            var currentUser = User.Identity as ClaimsIdentity;
            var ee = new UserViewModel();
            ee.Id = currentUser.Claims.Where(
                c => c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")
                .Select(c => c.Value).Single();
            ee.UserName = currentUser.Name;
            //var user = new UserViewModel(, );
            var identity = await _userManager.CreateIdentityAsync(ee, DefaultAuthenticationTypes.ApplicationCookie);

            //Create Claim local for repository
            Claim claim;
            if (identity.Claims.Count(c => c.Type == "locale") != 0)
            {
                claim = (from c in identity.Claims
                         where c.Type == "locale"
                         select c).Single();
                identity.RemoveClaim(claim);
            }
            if (identity.Claims.Count(c => c.Type == "TimeZone") != 0)
            {
                claim = (from c in identity.Claims
                         where c.Type == "TimeZone"
                         select c).Single();
                identity.RemoveClaim(claim);
            }
            MvcApplication.Log.Info("Delete claim");
            //Create Claim local for repository
            identity.AddClaim(new Claim("locale", (loc).ToString()));
            identity.AddClaim(new Claim("TimeZone", (time).ToString()));
            MvcApplication.Log.Info("Create Custom Claim local for repository");
            AuthenticationManager.SignIn(identity);
            if (identity.Claims.Count(c => c.Type == "locale") != 0)
            {
                var cultureInfo = new CultureInfo(identity.Claims.Where(c => c.Type == "locale").Select(c => c.Value).SingleOrDefault());
                Response.Cookies.Add(new HttpCookie("Lang", cultureInfo.Name));
                var timeZone = identity.Claims.Where(c => c.Type == "TimeZone").Select(c => c.Value).SingleOrDefault();
                Response.Cookies.Add(new HttpCookie("Time", timeZone));
                MvcApplication.Log.Info("Change Language " + cultureInfo);
            }
            else
            {
                var cultureInfo = new CultureInfo("ru-RU");
                Response.Cookies.Add(new HttpCookie("Lang", cultureInfo.Name));
                Response.Cookies.Add(new HttpCookie("Time", TimeZoneInfo.Local.DisplayName));
                MvcApplication.Log.Info("Change Language " + cultureInfo);
            }

            MvcApplication.Log.Info("Login successful USER : " + identity.Name);
            return RedirectToAction("Index", "Home");
        }
        [Authorize]
        public ActionResult Logout()
        {
            MvcApplication.Log.Info("Logout successful, USER : " + User.Identity.Name);
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Unauthorized(string returnUrl)
        {
            return View();
        }
    }
}