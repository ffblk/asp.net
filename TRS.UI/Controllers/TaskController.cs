﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using TRS.UI.Models;
using AutoMapper;
using System.Security.Claims;
using System.Threading;
using Microsoft.AspNet.Identity;
using TRS.UI.Infrastructure;
using TRS.UI.ServiceReferenceTask;
using TRS.UI.ServiceReferenceUser;
using TRS.UI.ServiceReferenceUserRole;


namespace TRS.UI.Controllers
{
    public class TaskController : Controller
    {
        // GET: Project
        private readonly ServiceReferenceTask.ITaskService _taskService;
        private readonly ServiceReferenceUserRole.IUserRoleService _userRoleService;
        private readonly ServiceReferenceUser.IUserInfoService _userService;
        public TaskController()
        {
            _taskService = new TaskServiceClient();
            _userRoleService = new UserRoleServiceClient();
            _userService = new UserInfoServiceClient();
        }
        public ActionResult AllTasks(Guid token)
        {
            ServiceReferenceTask.Task[] tasks = null;
            var claimsIdentity = User.Identity as ClaimsIdentity;
            if (claimsIdentity == null) return View(Mapper.Map<IEnumerable<TaskViewModel>>(tasks));
            if (claimsIdentity.HasClaim("Manager", token.ToString()) 
                || claimsIdentity.HasClaim(ClaimTypes.Role,"admin"))
                tasks = _taskService.ShowTasks(token);
            else if (claimsIdentity.HasClaim("User", token.ToString()))
            {
                var dd = _taskService.ShowTasks(token);
                var ff = Mapper.Map<IEnumerable<UI.Models.Task>>(dd);
                tasks = Mapper.Map<ServiceReferenceTask.Task[]>(ff.Where(tsk => (claimsIdentity.HasClaim("User", tsk.Id.ToString()) == true)));

                //_taskService.ShowTasks(token)
                //        .Where(tsk => (claimsIdentity.HasClaim("User", tsk.Id.ToString()) == true));
            }
            else throw new Exception("ACCESS DENIED");
            var viewModel = Mapper.Map<IEnumerable<TaskViewModel>>(tasks);
            ViewBag.Project = token;
            return View(viewModel);
        }
        public ActionResult CreateTask(string token)
        {
            ViewBag.List =  UsersHelper.GetAvaliableLocalizations(_userService);
            var currentUser = Thread.CurrentPrincipal.Identity as ClaimsIdentity;
            var task = _taskService.ShowTaskDetails(Guid.Parse(token));
            var dd = Mapper.Map<UI.Models.Task>(task);
            dd.Creator = Guid.Parse(currentUser.GetUserId());
            return View(Mapper.Map<TaskViewModel>(dd));
        }
        [HttpPost]
        public ActionResult CreateTask(TaskViewModel task)
        {
            if (task.Managers == null
                || (task.StartDate > task.EndDate)
                || _taskService.ShowTaskName(task.Name))
            {
                ViewBag.List = UsersHelper.GetAvaliableLocalizations(_userService);
                return View(task);
            }
            var domainTask = Mapper.Map<ServiceReferenceTask.Task>(task);
            _taskService.EditTask(domainTask);
            _userRoleService.UpdateRoles(task.Id, task.Managers.ToArray(), 2);
            _userRoleService.UpdateRoles(task.ProjectId, task.Managers.ToArray(), 2);
            return RedirectToAction("Index", "Home");
        }
    }
}