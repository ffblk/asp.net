﻿using System;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using TRS.UI.Infrastructure;
using TRS.UI.Models;
using TRS.UI.ServiceReferenceUser;


namespace TRS.UI.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        private readonly ServiceReferenceUser.IUserInfoService _userService;
        private readonly ServiceReferenceProject.IProjectService _projectService;
        private readonly ServiceReferenceUserRole.IUserRoleService _userRoleService;

        public AdminController(/*IUserService carService*/)
        {
            _userService = new UserInfoServiceClient();
            _projectService = new ServiceReferenceProject.ProjectServiceClient();
            _userRoleService = new ServiceReferenceUserRole.UserRoleServiceClient();
        }
        public ActionResult CreateProject(string token)
        {
            ViewBag.List = UsersHelper.GetAvaliableLocalizations(_userService);
            var project = token!= null ? _projectService.ShowProjectDetails(Guid.Parse(token)) : new ServiceReferenceProject.Project();
            if (token != null)
                MvcApplication.Log.Info("Edit Project "+token);
            else
                MvcApplication.Log.Info("Create New Project");
            return View(Mapper.Map<ProjectViewModel>(project));
        }
        [HttpPost]
        public ActionResult CreateProject(ProjectViewModel project)
        {
            if (project.Managers == null
                || (project.StartDate > project.EndDate)
                || _projectService.ShowProjectName(project.Name))
            {
                ViewBag.List = UsersHelper.GetAvaliableLocalizations(_userService);
                MvcApplication.Log.Info("Invalid data in Create Project View");
                return View(project);
            }
            var domainTask = Mapper.Map<ServiceReferenceProject.Project>(project);
            _projectService.EditProject(domainTask);
            _userRoleService.UpdateRoles(project.Id, project.Managers.ToArray(),1);
            MvcApplication.Log.Info("Create Project successful");
            return RedirectToAction("Index", "Home");
        }
    }
}